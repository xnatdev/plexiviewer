/*
 * org.nrg.xnat.plexiviewer.reader.ImageReader
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.reader;

import ij.io.FileInfo;

import java.io.IOException;

public interface ImageReader {
    public FileInfo getFileInfo() throws IOException;

    public String getOrientation();

    public int getOrientationForWriter();

    public int getVolumes();

    public boolean isZipped();

    public void clearTempFolder();
}
