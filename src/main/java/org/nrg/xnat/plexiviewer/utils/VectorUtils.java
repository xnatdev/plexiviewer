/*
 * org.nrg.xnat.plexiviewer.utils.VectorUtils
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.utils;

/**
 * @author Mohana
 */

import java.util.Vector;

public class VectorUtils {

    public static Vector intersect(Vector v1, Vector v2) {
        Vector rtn = new Vector();
        for (int i = 0; i < v1.size(); i++) {
            if (v2.contains(v1.elementAt(i))) {
                rtn.add(v1.get(i));
            }
        }
        return rtn;
    }

}
