/*
 * org.nrg.xnat.plexiviewer.utils.PlexiConstants
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.utils;

public class PlexiConstants {
    public static final String PLEXI_DEFAULT_SPEC       = "PLEXI_DEFAULT";
    public static final String XNAT_IMAGERESOURCE       = "xnat:imageResource";
    public static final String XNAT_IMAGERESOURCESERIES = "xnat:imageResourceSeries";
    public static final String XNAT_DICOMSERIES         = "xnat:dicomSeries";
    public static final String PLEXI_IMAGERESOURCE      = "plexi:imageResource";
    public static final String XNAT_RESOURCECATALOG     = "xnat:resourceCatalog";
}
