/*
 * org.nrg.xnat.plexiviewer.utils.XnatLoginDetails
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.utils;

import org.nrg.xdat.security.XDATUser;

import javax.servlet.http.HttpServletRequest;

public class XnatLoginDetails {
    public XDATUser getSessionUser(HttpServletRequest httpRequest) {
        return (XDATUser) httpRequest.getSession().getAttribute("user");
    }

}
