/*
 * org.nrg.xnat.plexiviewer.exceptions.InvalidParameterValueException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.exceptions;

public class InvalidParameterValueException extends Exception {
    public InvalidParameterValueException() {
    }

    public InvalidParameterValueException(String msg) {
        super("Invalid:: " + msg);
    }
}	
