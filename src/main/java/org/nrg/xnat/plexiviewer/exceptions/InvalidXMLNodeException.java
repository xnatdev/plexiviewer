/*
 * org.nrg.xnat.plexiviewer.exceptions.InvalidXMLNodeException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.exceptions;

/**
 * @author Mohana
 */
public class InvalidXMLNodeException extends Exception {

    public InvalidXMLNodeException() {
    }

    public InvalidXMLNodeException(String msg) {
        super("Dont know how to deal with element :: " + msg);
    }

}
