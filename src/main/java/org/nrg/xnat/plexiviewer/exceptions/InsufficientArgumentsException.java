/*
 * org.nrg.xnat.plexiviewer.exceptions.InsufficientArgumentsException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.exceptions;

public class InsufficientArgumentsException extends Exception {
    public InsufficientArgumentsException() {
    }

    public InsufficientArgumentsException(String msg) {
        super(msg);
    }
}
