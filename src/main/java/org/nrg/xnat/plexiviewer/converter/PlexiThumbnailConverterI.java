/*
 * org.nrg.xnat.plexiviewer.converter.PlexiThumbnailConverterI
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.converter;

import org.nrg.xnat.plexiviewer.lite.UserSelection;

public interface PlexiThumbnailConverterI {
    int convertAndSave(UserSelection options);
}