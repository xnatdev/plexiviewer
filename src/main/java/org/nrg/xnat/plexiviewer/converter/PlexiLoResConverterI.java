/*
 * org.nrg.xnat.plexiviewer.converter.PlexiLoResConverterI
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.converter;

import org.nrg.xnat.plexiviewer.lite.UserSelection;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;

import java.net.URISyntaxException;

public interface PlexiLoResConverterI {
    public int convertAndSave(UserSelection options);

    public PlexiImageFile getFileLocationAndName() throws URISyntaxException, Exception;
}
