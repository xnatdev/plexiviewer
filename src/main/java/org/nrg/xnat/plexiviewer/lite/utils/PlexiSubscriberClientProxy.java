/*
 * org.nrg.xnat.plexiviewer.lite.utils.PlexiSubscriberClientProxy
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.utils;

import org.nrg.xnat.plexiviewer.lite.gui.PlexiMessagePanel;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class PlexiSubscriberClientProxy extends Thread {
    PlexiMessagePanel msgPanel;
    boolean update = false;
    String opt;
    private URL           dataURL;
    private URLConnection servletConnection;
    private boolean stop = false;
    private int     inc;
    private boolean isConnected;

    public PlexiSubscriberClientProxy(PlexiMessagePanel mPanel, String opt) {
        this.opt = opt;
        msgPanel = mPanel;
        inc = 1;
        isConnected = false;
    }

    private void openConnection() {
        String suffix = HTTPDetails.getSuffix("PublisherServlet");
        try {
            dataURL = HTTPDetails.getURL(HTTPDetails.host, suffix);
            servletConnection = HTTPDetails.openConnection(dataURL);
            servletConnection.setDoInput(true);
            servletConnection.setDoOutput(true);
            //Don't use a cached version of URL connection.
            servletConnection.setUseCaches(false);
            servletConnection.setDefaultUseCaches(false);
            //Specify the content type that we will send binary data
            servletConnection.setRequestProperty("Content-Type", "application/octet-stream");
            ObjectOutputStream outStreamToServlet = new ObjectOutputStream(servletConnection.getOutputStream());
            outStreamToServlet.writeObject(opt);
            outStreamToServlet.flush();
            outStreamToServlet.close();
        } catch (MalformedURLException mfe) {
            mfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    public void run() {
        while (!stop) {
            try {
                //inc=inc+1;
                //msgPanel.showProgress(inc);
                retrieveMessage();
                sleep(40);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public void finish() {
        stop = true;
        //msgPanel.resetMessages();
        //super.stop();
    }

    private void retrieveMessage() throws IOException {
        //System.out.println("Isconnected " + isConnected + " " + inc);
        openConnection();
        InputStream       is = servletConnection.getInputStream();
        ObjectInputStream in = new ObjectInputStream(is);
        try {
            Object message = in.readObject();
            //System.out.println("Client Proxy recd msg " + message);
            msgPanel.setMessage((String) message);
        } catch (Exception e) {
            msgPanel.setMessage("");
        }
        is.close();
        in.close();
    }
}
