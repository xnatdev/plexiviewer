/*
 * org.nrg.xnat.plexiviewer.lite.viewer.miriadlobarseg.SmallLabel
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.viewer.miriadlobarseg;

import java.awt.*;

class SmallLabel extends java.awt.Label {
    Dimension d;

    SmallLabel(int w, int h) {
        super();
        d = new Dimension(w, h);
    }

    public void setSize(int w, int h) {
        super.setSize(w, h);
        d.setSize(w, h);
    }

    public Dimension getPreferredSize() {
        Dimension s = super.getPreferredSize();
        //return s;
        //System.out.println("Small label preferred size: " + d);
        return d;
    }


    public void update(Graphics g) {
        paint(g);
    }


}
