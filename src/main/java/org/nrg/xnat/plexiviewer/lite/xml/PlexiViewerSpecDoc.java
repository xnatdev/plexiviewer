/*
 * org.nrg.xnat.plexiviewer.lite.xml.PlexiViewerSpecDoc
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.lite.xml;

import org.nrg.xnat.plexiviewer.utils.PlexiConstants;

import java.util.Hashtable;

public class PlexiViewerSpecDoc {
    private Hashtable viewerSpecForSession;

    public PlexiViewerSpecDoc() {
        viewerSpecForSession = new Hashtable();
    }

    public synchronized void setViewerSpecification(String project, PlexiViewerSpecForSession spec) {
        System.out.println("Setting for Session type:" + project + ":");
        spec.setProject(project);
        viewerSpecForSession.put(project, spec);
    }

    public PlexiViewerSpecForSession getPlexiViewerSpecForSession(String sessionType) {
        if ((sessionType == null) || !viewerSpecForSession.containsKey(sessionType)) {
            System.out.println("Couldnt find the PlexiSpec for sessionType :" + sessionType + ": returning default");
            PlexiViewerSpecForSession rtn = (PlexiViewerSpecForSession) viewerSpecForSession.get(PlexiConstants.PLEXI_DEFAULT_SPEC);
            if (rtn == null) {
                return new PlexiViewerSpecForSession();
            } else {
                return rtn;
            }
        } else {
            return (PlexiViewerSpecForSession) viewerSpecForSession.get(sessionType);
        }
    }


}
