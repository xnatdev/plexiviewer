/*
 * org.nrg.xnat.plexiviewer.lite.DBFSXMLInfo
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.lite;

/**
 * @author Mohana
 */

import org.nrg.xnat.plexiviewer.lite.ui.GenericIdentifier;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

public class DBFSXMLInfo implements Serializable {
    GenericIdentifier viType_displayName;
    Vector            scanNos;
    Vector            fsxmlmergedInfo;
    Hashtable         viewableOrienations;


    public DBFSXMLInfo() {
        fsxmlmergedInfo = new Vector();
        scanNos = new Vector();
        viewableOrienations = new Hashtable();
    }

    /**
     * @return
     */
    public Vector getFsxmlmergedInfo() {
        return fsxmlmergedInfo;
    }

    /**
     * @return
     */
    public Vector getScanNos() {
        return scanNos;
    }

    /**
     * @param v
     */
    public void setFsxmlmergedInfo(Vector v) {
        fsxmlmergedInfo = v;
    }

    /**
     * @param vector
     */
    public void setScanNos(Vector vector) {
        scanNos = vector;
    }

    /**
     * @return
     */
    public String getScanToDisplay() {
        return (String) viType_displayName.getValue();
    }

    /**
     * @return
     */
    public GenericIdentifier getTag() {
        return (GenericIdentifier) viType_displayName;
    }


    /**
     * @param g
     */
    public void setTag(GenericIdentifier g) {
        viType_displayName = g;
    }

    public Vector getLinkedDropDown() {
        Vector rtn = new Vector();
        for (int i = 0; i < this.fsxmlmergedInfo.size(); i++) {
            rtn.addElement(((FSXMLInfo) fsxmlmergedInfo.elementAt(i)).getDataToDisplay());
        }
        return rtn;
    }

    public void addScanNo(Object obj) {
        scanNos.addElement(obj);
    }

    /**
     * @return Returns the viewableOrienations.
     */
    public Vector getViewableOrienations(Object scanNo) {
        Vector rtn = new Vector();
        if (viewableOrienations.containsKey(scanNo)) {
            rtn = (Vector) viewableOrienations.get(scanNo);
        }
        return rtn;
    }

    /**
     * @param viewableOrienations The viewableOrienations to set.
     */
    public void setViewableOrienations(Object scanNo, Vector viewableOrienations) {
        this.viewableOrienations.put(scanNo, viewableOrienations);
    }

    public String toString() {
        String rtn = "DISPLAY " + getScanToDisplay();
        rtn += "Scan Nos: " + getScanNos();
        return rtn;
    }

}
