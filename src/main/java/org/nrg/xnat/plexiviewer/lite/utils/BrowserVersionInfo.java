/*
 * org.nrg.xnat.plexiviewer.lite.utils.BrowserVersionInfo
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.lite.utils;

public class BrowserVersionInfo {
    boolean isMac;
    boolean isWin;
    String  java_version;
    String  mrj_version;

    public BrowserVersionInfo() {
        String osname = System.getProperty("os.name");
        isWin = osname.startsWith("Windows");
        isMac = !isWin && osname.startsWith("Mac");
    }

    public String getMRJVersion() {
        return System.getProperty("mrj.version");
    }

    public String getJavaVersion() {
        return System.getProperty("java.version");
    }

    public boolean isMac() {
        return isMac;
    }

    public boolean isWin() {
        return isWin;
    }


}
