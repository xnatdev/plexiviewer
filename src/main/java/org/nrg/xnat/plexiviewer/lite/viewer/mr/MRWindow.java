/*
 * org.nrg.xnat.plexiviewer.lite.viewer.mr.MRWindow
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.viewer.mr;

public interface MRWindow {

    public int getCurrentTool();

    public void setMessage(String d, String m);

    public void message();

}

